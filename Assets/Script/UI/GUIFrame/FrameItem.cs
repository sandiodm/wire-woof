﻿using UnityEngine;
using System.Collections;

public class FrameItem : MonoBehaviour
{
    public bool AutoResize = false;

    private Camera _camera;

    private Vector3 _defaultsize;
    private Vector3 _currentsize;

    void Start()
    {
        Camera cam = transform.parent.GetComponent<GUIFrame>().GUICamera;
        if (cam != null)
            _camera = cam;
        else
        {
            _camera = Camera.main;
            print(this.gameObject.name + " can't find camera. will be set to main camera");
        }

        _defaultsize = this.transform.localScale;
        _currentsize = _defaultsize;
        initSize();
    }
    
    void Update()
    {

    }

    //public void initSize()
    //{
    //    if (Camera.main.aspect >= 0)
    //    {
    //        float defaultW = 800.0f / 480.0f;
    //        float currentW = _camera.aspect;

    //        _currentsize.x = _defaultsize.x * (defaultW / currentW);

    //        this.transform.localScale = _currentsize;
    //    }
    //    else
    //    {
    //        float defaultH = 480.0f / 800.0f;
    //        float currentH = 1.0f / _camera.aspect;

    //        _currentsize.y = _defaultsize.y * (defaultH / currentH);

    //        this.transform.localScale = _currentsize;
    //    }
    //}

    public void initSize()
    {
        if (Camera.main.aspect >= 0)
        {
            if (AutoResize)
                adjustHeight();
            else
                adjustWidth();
        }
        else
        {
            if (AutoResize)
                adjustWidth();
            else
                adjustHeight();
        }
    }

    private void adjustWidth()
    {
        float defaultW = 800.0f / 480.0f;
        float currentW = _camera.aspect;

        _currentsize.x = _defaultsize.x * (defaultW / currentW);

        this.transform.localScale = _currentsize;
    }

    private void adjustHeight()
    {
        float defaultH = 480.0f / 800.0f;
        float currentH = 1.0f / _camera.aspect;

        _currentsize.y = _defaultsize.y * (defaultH / currentH);

        this.transform.localScale = _currentsize; 
    }
}