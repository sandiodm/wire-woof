﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GUIFrame : MonoBehaviour
{

    public bool LastUpdate = false;

    [HideInInspector]
    public Camera GUICamera;

    protected Vector3 _size;
    private Rect _rectangle;

    void Awake()
    {
        try
        {
            GUICamera = this.transform.parent.camera;
        }
        catch
        {
            Debug.Log("Parent doesn't have camera");
        }

        initSize(GUICamera.orthographicSize, GUICamera.aspect);
        _rectangle = new Rect(0, 0, 0, 0);
    }

    void Update()
    {
        if (!LastUpdate)
        {
            _rectangle.x = this.transform.position.x - (_size.x / 2.0f);
            _rectangle.y = this.transform.position.y - (_size.y / 2.0f);
            _rectangle.width = _size.x;
            _rectangle.height = _size.y;
        }
    }

    void LateUpdate()
    {
        if (LastUpdate)
        {
            _rectangle.x = this.transform.position.x - (_size.x / 2.0f);
            _rectangle.y = this.transform.position.y - (_size.y / 2.0f);
            _rectangle.width = _size.x;
            _rectangle.height = _size.y;
        }
    }

    public void initSize(float orthographicSize, float aspectRatio)
    {
        float hSize = orthographicSize * 2.0f;
        float vSize = aspectRatio * hSize;

        _size = new Vector3(vSize, hSize, 1);

        this.transform.localScale = _size;
        _rectangle.width = _size.x;
        _rectangle.height = _size.y;
    }

    public Rect Rectangle
    {
        get { return _rectangle; }
    }
}
