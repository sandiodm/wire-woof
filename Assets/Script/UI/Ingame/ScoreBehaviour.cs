﻿using UnityEngine;
using System.Collections;

public class ScoreBehaviour : MonoBehaviour
{
    private TextMesh _text = null;

    void Awake()
    {
        _text = this.GetComponent<TextMesh>();
    }

    // Update is called once per frame
    void Update()
    {
        if (StatePlayer.GameManager != null)
        {
            _text.text = "" + StatePlayer.GameManager.Score;
        }
    }
}
