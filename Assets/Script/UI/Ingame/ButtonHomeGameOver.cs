﻿using UnityEngine;
using System.Collections;

public class ButtonHomeGameOver : MonoBehaviour
{
    private Vector3 _originalscale;

    void Awake()
    {
        _originalscale = this.transform.localScale;
    }

    void OnMouseDown()
    {
        this.transform.localScale = _originalscale * 0.9f;
    }

    void OnMouseUp()
    {
        this.transform.localScale = _originalscale;
    }

    void OnMouseUpAsButton()
    {
        if (StatePlayer.GameManager.State == GameState.GameOver)
            StartCoroutine("click");
    }

    IEnumerator click()
    {
        StatePlayer.GameManager.State = GameState.None;
        //..play sfx
        yield return new WaitForSeconds(0.25f);
        StatePlayer.GameManager.BackToHome();
    }
}
