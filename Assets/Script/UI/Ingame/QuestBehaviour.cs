﻿using UnityEngine;
using System.Collections;

public class QuestBehaviour : MonoBehaviour
{
    private TextMesh _text;

    void Awake()
    {
        _text = this.GetComponent<TextMesh>();
    }

    public void SetAnswer(string answer)
    {
        this._text.text = "" + answer;
    }
}
