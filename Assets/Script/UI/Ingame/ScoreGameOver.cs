﻿using UnityEngine;
using System.Collections;

public class ScoreGameOver : MonoBehaviour
{
    void OnEnable()
    {
        this.GetComponent<TextMesh>().text = "" + StatePlayer.GameManager.Score;
    }
}
