﻿using UnityEngine;
using System.Collections;

public class BestScoreGameOver : MonoBehaviour
{
    void OnEnable()
    {
        if (StatePlayer.GameManager.Score > PlayerData.BestScore)
            PlayerData.BestScore = StatePlayer.GameManager.Score;

        this.GetComponent<TextMesh>().text = "" + PlayerData.BestScore;
    }
}
