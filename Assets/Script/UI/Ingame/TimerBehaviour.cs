﻿using UnityEngine;
using System.Collections;

public class TimerBehaviour : MonoBehaviour
{
    private TextMesh _text = null;

    void Awake()
    {
        _text = this.GetComponent<TextMesh>();
    }

    // Update is called once per frame
    void Update()
    {
        if (StatePlayer.GameManager != null)
        {
            if ((int)StatePlayer.GameManager.CurrentTime / 10 > 0)
                _text.text = "" + ((int)StatePlayer.GameManager.CurrentTime);
            else
                _text.text = "0" + ((int)StatePlayer.GameManager.CurrentTime);
        }
    }
}
