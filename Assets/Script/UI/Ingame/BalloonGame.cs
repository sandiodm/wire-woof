﻿using UnityEngine;
using System.Collections;

public class BalloonGame : MonoBehaviour
{
    public TextMesh Text;

    void Awake()
    {

    }

    public void SetAnswer(string answer)
    {
        if (string.IsNullOrEmpty(answer))
            this.Text.text = "";
        else
            this.Text.text = generateRandomAnswer(answer);
    }

    private string generateRandomAnswer(string answer)
    {
        string val = "cut the " + answer;
        val += "\n one";
        int rand = Random.Range(0, 2);

        switch (rand)
        {
            case 0:
                val = "cut the " + answer;
                val += "\n one. Fast!";
                break;
            case 1:
                val = "cut the " + answer;
                val += "\n one";
                break;
            case 2:
                val = "i repeat, cut";
                val += "\n the " + answer + " one";
                break;
            case 3:
                val = "can you see";
                val += "\n the " + answer + " one?";
                break;
            case 4:
                val = "can you see";
                val += "\n the " + answer + " one?";
                break;
            default:
                break;
        }

        return val;
    }
}
