﻿using UnityEngine;
using System.Collections;

public class BalloonMenu : MonoBehaviour
{
    public TextMesh Text;

    private float _changeinterval = 3.0f;
    private float _currentinterval = 0;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (_currentinterval > 0)
        {
            _currentinterval -= Time.deltaTime;
        }
        else
        {
            _currentinterval = _changeinterval;
            Text.text = generateRandomString();
        }
    }

    private string generateRandomString()
    {
        string val = ". . .";
        int rand = Random.Range(0, 8);

        switch (rand)
        {
            case 0:
                val = "where is your";
                val += "\nkey, Jeff?";
                break;
            case 1:
                val = "I love";
                val += "\nmonster truck";
                break;
            case 2:
                val = "Don't spill";
                val += "\nyour coffee";
                break;
            case 3:
                val = "Wear your";
                val += "\njacket, Jeff";
                break;
            case 4:
                val = "Eat your";
                val += "\nsalad";
                break;
            case 5:
                val = "....bzzztt";
                break;
            case 6:
                val = "munch..";
                val += "\nmunch....";
                break;
            default:
                break;
        }

        return val;
    }
}
