﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ButtonCredit : MonoBehaviour
{
    public Transform CreditContent;
    public List<Transform> OtherContents;

    private Vector3 _originalscale;


    void Awake()
    {
        _originalscale = this.transform.localScale;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && StatePlayer.MenuManager.State == MenuState.Credit)
            StartCoroutine("closecredit");
    }

    void OnMouseDown()
    {
        this.transform.localScale = _originalscale * 0.9f;
    }

    void OnMouseUp()
    {
        this.transform.localScale = _originalscale;
    }

    void OnMouseUpAsButton()
    {
        if (StatePlayer.MenuManager.State == MenuState.MainMenu)
            StartCoroutine("opencredit");
        else if (StatePlayer.MenuManager.State == MenuState.Credit)
            StartCoroutine("closecredit");
    }

    IEnumerator opencredit()
    {
        StatePlayer.MenuManager.State = MenuState.None;
        this.transform.localScale = _originalscale;
        StatePlayer.MenuManager.Fader.Play("in", 0, 0);
        yield return new WaitForSeconds(0.4f);
        foreach (Transform t in OtherContents)
            t.gameObject.SetActive(false);
        CreditContent.gameObject.SetActive(true);
        StatePlayer.MenuManager.Fader.Play("out", 0, 0);
        yield return new WaitForSeconds(0.3f);
        StatePlayer.MenuManager.State = MenuState.Credit;
    }

    IEnumerator closecredit()
    {
        StatePlayer.MenuManager.State = MenuState.None;
        this.transform.localScale = _originalscale;
        StatePlayer.MenuManager.Fader.Play("in", 0, 0);
        yield return new WaitForSeconds(0.4f);
        foreach (Transform t in OtherContents)
            t.gameObject.SetActive(true);
        CreditContent.gameObject.SetActive(false);
        StatePlayer.MenuManager.Fader.Play("out", 0, 0);
        yield return new WaitForSeconds(0.3f);
        StatePlayer.MenuManager.State = MenuState.MainMenu;
    }
}
