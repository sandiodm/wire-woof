﻿using UnityEngine;
using System.Collections;

public class ButtonPlay : MonoBehaviour
{
    private Vector3 _originalscale;


    void Awake()
    {
        _originalscale = this.transform.localScale;
    }
    
    void OnMouseDown()
    {
        this.transform.localScale = _originalscale * 0.9f;
    }

    void OnMouseUp()
    {
        this.transform.localScale = _originalscale;
    }

    void OnMouseUpAsButton()
    {
        if (StatePlayer.MenuManager.State == MenuState.MainMenu)
            StartCoroutine("click");
    }

    IEnumerator click()
    {
        StatePlayer.MenuManager.State = MenuState.None;
        this.transform.localScale = _originalscale;
        StatePlayer.SoundManager.PlayButton();
        yield return new WaitForSeconds(0.1f);
        StatePlayer.MenuManager.PlayGame();

    }
}
