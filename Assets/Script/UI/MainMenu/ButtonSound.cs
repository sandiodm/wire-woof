﻿using UnityEngine;
using System.Collections;

public class ButtonSound : MonoBehaviour
{
    public Sprite EnableSprite;
    public Sprite DisableSprite;

    private Vector3 _originalscale;
    private SpriteRenderer _renderer;
    

    void Awake()
    {
        _originalscale = this.transform.localScale;
        _renderer = this.GetComponent<SpriteRenderer>();
    }

    void OnEnable() 
    {
        updateButton();
    }

    void OnMouseDown()
    {
        this.transform.localScale = _originalscale * 0.9f;
    }

    void OnMouseUp()
    {
        this.transform.localScale = _originalscale;
    }

    void OnMouseUpAsButton()
    {
        if (StatePlayer.MenuManager.State == MenuState.MainMenu)
            StartCoroutine("click");
    }

    IEnumerator click()
    {
        StatePlayer.MenuManager.State = MenuState.None;
        this.transform.localScale = _originalscale;
        PlayerData.EnableSound = !PlayerData.EnableSound;
        updateButton();

        if (PlayerData.EnableSound)
        {
            StatePlayer.SoundManager.PlayBGM();
            StatePlayer.SoundManager.PlayButton();
        }
        else
            StatePlayer.SoundManager.StopBGM();

        yield return new WaitForSeconds(0.1f);
        updateButton();
        StatePlayer.MenuManager.State = MenuState.MainMenu;
    }

    private void updateButton()
    {
        if (PlayerData.EnableSound)
            _renderer.sprite = EnableSprite;
        else
            _renderer.sprite = DisableSprite;
    }
}
