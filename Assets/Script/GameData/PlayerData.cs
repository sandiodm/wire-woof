﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class PlayerData
{
    public static List<GameColor> GameColors = defaultColor();

    private static List<GameColor> defaultColor()
    {
        List<GameColor> val = new List<GameColor>();
        val.Add(new GameColor(ColorType.Red, new Color(98.0f, 98.0f, 98.0f, 255.0f) / 255.0f));
        val.Add(new GameColor(ColorType.Green, new Color(106.0f, 106.0f, 106.0f, 255.0f) / 255.0f));
        val.Add(new GameColor(ColorType.Blue, new Color(71.0f, 71.0f, 71.0f, 255.0f) / 255.0f));
        val.Add(new GameColor(ColorType.Yellow, new Color(165.0f, 165.0f, 165.0f, 255.0f) / 255.0f));
        val.Add(new GameColor(ColorType.Purple, new Color(96.0f, 96.0f, 96.0f, 255.0f) / 255.0f));

        return val;
    }

    public static int BestScore
    {
        get
        {
            return PlayerPrefs.GetInt("score", 0);
        }

        set
        {
            PlayerPrefs.SetInt("score", value);
        }
    }

    public static bool EnableSound
    {
        get
        {
            int setting = PlayerPrefs.GetInt("soundsetting", 1);
            return (setting > 0) ? true : false;
        }

        set
        {
            int setting = (value == true) ? 1 : 0;
            PlayerPrefs.SetInt("soundsetting", setting);
        }
    }
}
