﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour
{
    public MenuState State;
    public Animator Fader;
        
    void Awake()
    {
        StatePlayer.MenuManager = this;
    }

    void Start()
    {
        State = MenuState.MainMenu;
    }

    // Update is called once per frame
    void Update()
    {
        if (State == MenuState.MainMenu && Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    public void PlayGame()
    {
        StartCoroutine("playgame");
    }

    IEnumerator playgame()
    {
        this.State = MenuState.None;
        Fader.Play("in", 0, 0);
        yield return new WaitForSeconds(0.3f);
        this.camera.enabled = false;
        StatePlayer.GameManager.Score = 0;
        StatePlayer.GameManager.GameBalloon.SetAnswer(null);
        StatePlayer.GameManager.camera.enabled = true;
        Fader.Play("out", 0, 0);
        yield return new WaitForSeconds(1.0f);
        StatePlayer.GameManager.StartPlay();
    }
}
