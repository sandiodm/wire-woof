﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
    public AudioSource BGM;
    public AudioSource Button;
    public AudioSource Woof;
    public AudioSource Tick;
    public AudioSource Explosion;

    void Awake()
    {
        StatePlayer.SoundManager = this;
    }

    void Start()
    {
        PlayBGM();
    }

    public void PlayBGM()
    {
        if (PlayerData.EnableSound)
            BGM.Play();
    }

    public void StopBGM()
    {
        BGM.Stop();
    }

    public void PlayButton()
    {
        if (PlayerData.EnableSound)
            Button.Play();
    }

    public void StopButton()
    {
        Button.Stop();
    }

    public void PlayWoof()
    {
        if (PlayerData.EnableSound)
            Woof.Play();
    }

    public void StopWoof()
    {
        Woof.Stop();
    }

    public void PlayTick(float startTime)
    {
        if (PlayerData.EnableSound)
        {
            Tick.time = Mathf.Max(0, startTime);
            Tick.Play();
        }
    }

    public void StopTick()
    {
        Tick.Stop();
    }

    public void PlayExplosion()
    {
        if (PlayerData.EnableSound)
            Explosion.Play();
    }

    public void StopExplosion()
    {
        Explosion.Stop();
    }
}
