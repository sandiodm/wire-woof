﻿using UnityEngine;
using System.Collections;

public class ExplosionManager : MonoBehaviour
{
    public float Interval;
    
    void OnEnable()
    {
        Invoke("disableObject", Interval);
    }

    private void disableObject()
    {
        this.gameObject.SetActive(false);
    }
}
