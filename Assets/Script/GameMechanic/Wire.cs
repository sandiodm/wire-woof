﻿using UnityEngine;
using System.Collections;

public class Wire : MonoBehaviour
{
    public GameColor GameColor;
    public WireState State;
    public Sprite NormalSprite;
    public Sprite CutSprite;

    public void SetGameColor(GameColor gameColor)
    {
        this.GameColor = gameColor;
        this.GetComponent<SpriteRenderer>().color = gameColor.Index;
    }

    public void SetState(WireState state)
    {
        State = state;
        if (State == WireState.Cut)
            this.GetComponent<SpriteRenderer>().sprite = CutSprite;
        else if (State == WireState.Normal)
            this.GetComponent<SpriteRenderer>().sprite = NormalSprite;
    }

    public void SetSelected(bool value)
    {
        //..childnya adalah highlight
        this.transform.GetChild(0).gameObject.SetActive(value);
    }

    void OnMouseDown()
    {
        if (StatePlayer.GameManager != null)
        {
            if (StatePlayer.GameManager.State == GameState.Playing)
            {
                this.SetState(WireState.Cut);
                StatePlayer.GameManager.FeedAnswer(this.GameColor.Type);
            }
        }
    }
}
