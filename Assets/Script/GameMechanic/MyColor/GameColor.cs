﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class GameColor
{
    public ColorType Type;
    public Color Index;

    public GameColor(ColorType type, Color index)
    {
        this.Type = type;
        this.Index = index;
    }
}
