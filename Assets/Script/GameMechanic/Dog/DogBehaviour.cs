﻿using UnityEngine;
using System.Collections;

public class DogBehaviour : MonoBehaviour
{
    public DogState State;

    private Animator _animator;

    void Awake()
    {
        _animator = this.transform.GetComponent<Animator>();
        SetState(DogState.Idle);
    }

    public void SetState(DogState state)
    {
        this.State = state;
        switch (State)
        { 
            case DogState.Idle:
                _animator.Play("idle", 0, 0);
                break;
            case DogState.Worried:
                _animator.Play("worried", 0, 0);
                break;
            case DogState.Surprised:
                _animator.Play("surprised", 0, 0);
                break;
            case DogState.Relieved:
                _animator.Play("relieved", 0, 0);
                break;
            default:
                break;
        }
    }
}
