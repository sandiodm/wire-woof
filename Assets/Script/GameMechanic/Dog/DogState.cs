﻿
public enum DogState
{
    Idle,
    Worried,
    Surprised,
    Relieved
}