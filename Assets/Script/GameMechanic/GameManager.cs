﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public GameState State = GameState.None;
    public int Score = 0;
    public int CurrentSequence = 0;
    public float CurrentTime;
    public ColorType Answer;
    public ColorType Bomb;
    public DogBehaviour Dog;
    public Animator GameOverObject;
    public Transform GameOverBlank;
    public Transform Explosion;
    //public QuestBehaviour Question;
    public BalloonGame GameBalloon;
    public List<Transform> WirePositions;
    public List<Wire> Wires;

    private int _maxwire;
    private List<int> _activewire;
    private List<int> _inactivewire;


    void Awake()
    {
        StatePlayer.GameManager = this;
    }

    void Start()
    {
    }

    public void StartPlay()
    {
        Score = 0;
        CurrentSequence = 0;
        State = GameState.Playing;
        ShuffleWires();

        Dog.SetState(DogState.Idle);
        GameBalloon.gameObject.SetActive(true);
    }

    public void ShuffleWires()
    {
        _maxwire = getMaxWire();
        CurrentTime = getTimerInterval();
        StatePlayer.SoundManager.PlayTick(11.0f - CurrentTime);

        initWires(_maxwire);
        List<int> pid = generatePositionID(_maxwire);
        List<int> cid = generateColorID(_maxwire);

        initAnswer(cid);

        float zpos = 0;
        for (int i = 0; i < _activewire.Count; i++)
        {
            int posid = pid[i];
            int wirid = _activewire[i];
            int colid = cid[i];

            Wires[wirid].SetState(WireState.Normal);
            Wires[wirid].SetGameColor(PlayerData.GameColors[colid]);
            Vector3 pos = WirePositions[posid].position;
            pos.z += zpos;
            Wires[wirid].transform.position = pos;
            Wires[wirid].gameObject.SetActive(true);

            zpos += 0.5f;
        }

        for (int i = 0; i < _inactivewire.Count; i++)
        { 
            int wirid = _inactivewire[i];
            Wires[wirid].gameObject.SetActive(false);
        }

        //Question.SetAnswer("" + Answer);
        GameBalloon.SetAnswer("" + Answer);
        State = GameState.Playing;
    }

    void Update()
    {
        if (State == GameState.Playing)
        {
            if ((int)CurrentTime > 0)
            {
                if ((int)CurrentTime <= 1)
                {
                    if (Dog.State != DogState.Surprised)
                        Dog.SetState(DogState.Surprised);
                }
                else if ((int)CurrentTime <= 3)
                {
                    if (Dog.State != DogState.Worried)
                        Dog.SetState(DogState.Worried);
                }
                else
                {
                    if (Dog.State != DogState.Idle)
                        Dog.SetState(DogState.Idle);
                }

                CurrentTime -= Time.deltaTime;
            }
            else
                GameOver();
        }
    }

    private int getMaxWire()
    {
        int val = 2;

        if (CurrentSequence < 5)
            val = 2;
        else if (CurrentSequence < 10)
            val = 3;
        else if (CurrentSequence < 15)
            val = 4;
        else
            val = 5;

        return val;
    }

    private float getTimerInterval()
    {
        float val = 11.0f;

        if (CurrentSequence < 5)
            val = 9.0f;
        else if (CurrentSequence < 10)
            val = 7.0f;
        else if (CurrentSequence < 15)
            val = 5.0f;
        else
            val = 3.0f;

        return val;
    }

    private void initWires(int maxWire)
    {
        _activewire = new List<int>();
        _inactivewire = new List<int>();

        for (int i = 0; i < Wires.Count; i++)
            _inactivewire.Add(i);

        for (int i = 0; i < maxWire; i++)
        {
            int id = Random.Range(0, _inactivewire.Count);
            _activewire.Add(_inactivewire[id]);
            _inactivewire.RemoveAt(id);
        }
    }

    private List<int> generatePositionID(int maxWire)
    {
        List<int> val = new List<int>();

        List<int> temp = new List<int>();
        for (int i = 0; i < WirePositions.Count; i++)
            temp.Add(i);

        for (int i = 0; i < maxWire; i++)
        {
            int id = Random.Range(0, temp.Count);
            val.Add(temp[id]);
            temp.RemoveAt(id);
        }

        return val;
    }

    private List<int> generateColorID(int maxWire)
    {
        List<int> val = new List<int>();

        List<int> temp = new List<int>();
        for (int i = 0; i < PlayerData.GameColors.Count; i++) //kalo entar butuh spesifikasi warna, edit di sini
            temp.Add(i);

        for (int i = 0; i < maxWire; i++)
        {
            int id = Random.Range(0, temp.Count);
            val.Add(temp[id]);
            temp.RemoveAt(id);
        }

        return val;
    }

    private void initAnswer(List<int> colorID)
    {
        List<int> temp = new List<int>();
        for (int i = 0; i < colorID.Count; i++)
            temp.Add(colorID[i]);

        int tID = Random.Range(0, temp.Count);
        Answer = PlayerData.GameColors[temp[tID]].Type;
        temp.RemoveAt(tID);

        tID = Random.Range(0, temp.Count);
        Bomb = PlayerData.GameColors[temp[tID]].Type;
    }

    public void FeedAnswer(ColorType type)
    {
        if (type == this.Answer)
        {
            State = GameState.None;

            Score++;
            CurrentSequence++;
            StatePlayer.SoundManager.StopTick();
            StatePlayer.SoundManager.PlayWoof();
            Dog.SetState(DogState.Relieved);

            Invoke("ShuffleWires", 0.5f);
            //ShuffleWires();
        }
        else if(type==this.Bomb)
        {
            GameOver();
        }
    }

    private void GameOver()
    {
        StartCoroutine("gameover");
    }

    IEnumerator gameover()
    {
        State = GameState.None;
        StatePlayer.SoundManager.StopTick();
        Explosion.gameObject.SetActive(true);
        GameOverBlank.gameObject.SetActive(true);
        StatePlayer.SoundManager.PlayExplosion();
        if (Dog.State != DogState.Surprised)
            Dog.SetState(DogState.Surprised);
        GameBalloon.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.75f);
        GameOverObject.gameObject.SetActive(true);
        GameOverObject.Play("in");
        yield return new WaitForSeconds(0.75f);
        State = GameState.GameOver;
    }

    public void RetryGameOver()
    {
        StartCoroutine("retrygameover");
    }

    IEnumerator retrygameover()
    {
        State = GameState.None;
        GameOverObject.Play("out", 0, 0);
        yield return new WaitForSeconds(0.5f);
        StatePlayer.MenuManager.Fader.Play("in", 0, 0);
        yield return new WaitForSeconds(0.4f);
        GameOverBlank.gameObject.SetActive(false);
        disableActiveWire();
        Score = 0;
        //Question.SetAnswer("");
        GameBalloon.SetAnswer("");
        Dog.SetState(DogState.Idle);
        yield return new WaitForSeconds(0.2f);
        StatePlayer.MenuManager.Fader.Play("out", 0, 0);
        yield return new WaitForSeconds(1.0f);
        StartPlay();
    }

    public void BackToHome()
    {
        StartCoroutine("backtohome");
    }

    IEnumerator backtohome()
    {
        State = GameState.None;
        GameOverObject.Play("out", 0, 0);
        yield return new WaitForSeconds(0.5f);
        StatePlayer.MenuManager.Fader.Play("in", 0, 0);
        yield return new WaitForSeconds(0.4f);
        GameOverBlank.gameObject.SetActive(false);
        disableActiveWire();
        Score = 0;
        //Question.SetAnswer("");
        GameBalloon.SetAnswer("");
        Dog.SetState(DogState.Idle);
        this.camera.enabled = false;
        StatePlayer.MenuManager.camera.enabled = true;
        yield return new WaitForSeconds(0.2f);
        StatePlayer.MenuManager.Fader.Play("out", 0, 0);
        yield return new WaitForSeconds(1.0f);
        StatePlayer.MenuManager.State = MenuState.MainMenu;
    }

    private void disableActiveWire()
    {
        for (int i = 0; i < _activewire.Count; i++)
        {
            int id = _activewire[i];
            Wires[id].gameObject.SetActive(false);
        }
    }
}
