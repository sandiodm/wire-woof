﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]

public class SimpleSort : MonoBehaviour
{
    public string Layer = "Default";
    public float Sort = 0;

//    void Start()
//    {
//        gameObject.renderer.sortingLayerName = Layer;
//        gameObject.renderer.sortingOrder = (int)Sort;
//
//    }
//
//#if UNITY_EDITOR
    void Update()
    {
        gameObject.renderer.sortingLayerName = Layer;
        gameObject.renderer.sortingOrder = (int)Sort;
    }
//#endif
}
